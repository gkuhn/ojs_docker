### Docker compose for OJS 3.3.3-16

This is a clean database installation for OJS 3.3.3-16. 
After the installation, is possible to populate with data using a dump.sql file.

### Specifications

- webserver = alpine / apache
- OJS version = 3_3_0-16
- php version = 8.1
- db = mariadb:5.5
- The database will be created with a clean data
- 3 themes plugins/theme will be installed:
    --Default
    --TUDJournalsTheme 
    --TUDJournalsTheme_MenuLeft

### Installation - Step by step:
    1- Clone/Download this repository
    2- Copy the content of .env.TEMPLATE INTO A .env file in this repo by crunning the following command
    `$ mv .env.TEMPLATE .env` 
    3- Run the follow command to dowload the default ojs config file
    `$ source .env && wget "https://github.com/pkp/ojs/raw/${OJS_VERSION}/config.TEMPLATE.inc.php" -O ./volumes/config/config.inc.php`
    4 - Grant permission to the content inside volumes to apache user and group (uid 100 and gid 101 inside the container), execpt for db and logs/db folders that will be owned by mysql user and group (uid and gid 999).
    `$ chown 100:101 ./volumes -R`
    `$ chown 999:999 ./volumes/db -R`
    `$ chown 999:999 ./volumes/logs/db -R`

    5 - Run the container
    `$ docker-compose up`

    6 - Access http://127.0.0.1:8081 and continue through web installation process.

    - 6.1 Create a user and a password for you to access the dasboard after the installation

    - 6.2 Add the database connection by modifiyng the following options:

        Database driver: mysqli 
        Host: db 

        (change below with the info setted in environment variables at .env file)
        Username: $OJS_DB_NAME
        Password: $OJS_DB_PASSWORD (change with the password you set in your environment variables)
        Database name: $OJS_DB_NAME

    - 6.3 Uncheck "Beacon"

    - 6.4 Click Install OJS button

    For development enviroment only:
    7 Modify back the ownership to you for volumes/plugins/themes to be able to edit the files under themes folder. 
    `$ chown YOUR_USER:YOUR_GROUP ./volumes/plugins/themes -R`


### Populating the clean database with data:

After the installation, you can import the dump file for the database to populate it with data:

`$ docker exec -i ojs_db_container mysql -u root -p{$MYSQL_ROOT_PASSWORD} {$OJS_DB_NAME} < /path/to/dump.sql`

When it is done, you can login into docker mysql container to check if the import went well:

`$ docker exec -it ojs_db_container mysql -u root -p{$MYSQL_ROOT_PASSWORD}`

Let's use the ojs database:

`USE ojs;`

Run a sample Query:

`SELECT * authors;`

The table should be populate with many authors. When you are done, exit the MySQL shell:

`EXIT;`

If something went worng, will can check the logs:

`$ docker logs ojs_db_container`

#### Usefull docker commands:
- start containers 
`docker-compose start`
- stop containers 
`docker-compose stop`
- list running containers 
`docker ps`
- list all containers
`docker ps -a`
- remove a container
`docker rm CONTAINER_ID`
- list volumes
`docker volume list`
- list images
`docker images`
