<?php
import('lib.pkp.classes.plugins.ThemePlugin');
class TUDJournalsTheme extends ThemePlugin {

	/**s
	 * Load the custom styles for our theme
	 * @return null
	 */
	public function init() {
        $this->setParent('defaultthemeplugin');
        $this->addStyle('rvssssssssssssssss-stylesheet', 'styles/index.less');
		$this->addScript('main', 'js/main.js');
	}

	/**
	 * Get the display name of this theme
	 * @return string
	 */

	function getDisplayName() {
		return __('plugins.themes.tudojs.name');
	}

	/**
	 * Get the description of this plugin
	 * @return string
	 */
	function getDescription() {
		return __('plugins.themes.tudojs.description');
	}
}

?>
