let element = document.querySelector('html body.pkp_page_management.pkp_op_settings div#app.app div.app__body main.app__main div.app__page div.pkpTabs div#appearance.pkpTab.pkpTab--isActive div.pkpTabs.pkpTabs--side.-pkpClearfix div#appearance-setup.pkpTab.pkpTab--isActive form.pkpForm.-pkpClearfix div.pkpFormPages div.pkpFormPage.pkpFormPage--current fieldset.pkpFormGroup.-pkpClearfix div.pkpFormGroup__fields div.pkpFormGroup__localeGroup.-pkpClearfix div.pkpFormGroup__locale.pkpFormGroup__locale--isVisible div.pkpFormField.pkpFormField--upload.pkpFormField--uploadImage div#appearanceSetup-journalThumbnail-control-en_US.pkpFormField__control.pkpFormField--upload__control div.pkpFormField--upload__preview.-pkpClearfix div.pkpFormField--upload__details label.pkpFormFieldLabel');

if (element) {
    element.textContent = 'Journal Title'; 
} else {
    console.error('Element not found');
}
