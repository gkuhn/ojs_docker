<?php
import('lib.pkp.classes.plugins.ThemePlugin');
class TUDJournalsTheme_MenuLeft extends ThemePlugin {

	/**s
	 * Load the custom styles for our theme
	 * @return null
	 */
	public function init() {
        $this->setParent('defaultthemeplugin');
        $this->addStyle('qw-stylesheet', 'styles/index.less');
	}

	/**
	 * Get the display name of this theme
	 * @return string
	 */

	function getDisplayName() {
		return __('plugins.themes.tudojsmenul.name');
	}

	/**
	 * Get the description of this plugin
	 * @return string
	 */
	function getDescription() {
		return __('plugins.themes.tudojsmenul.description');
	}
}

?>